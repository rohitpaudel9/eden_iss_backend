<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('subsystems', 'SubsystemController@index');
Route::get('subsystems/{subsystem}', 'SubsystemController@show');

Route::get('pumps', 'PumpController@index');
Route::get('pumps/{pump}', 'PumpController@show');

Route::get('pump-data', 'PumpDataController@index');
Route::get('pump-data/{parameterId}', 'PumpDataController@dataWithParameterId');

Route::get('solenoids', 'SolenoidController@index');
Route::get('solenoids/{solenoid}', 'SolenoidController@show');

