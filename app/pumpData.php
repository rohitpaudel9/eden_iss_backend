<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class pumpData extends Model
{
    protected $guarded = ['id', 'pumpable_id', 'pumpable_type', 'created_at', 'updated_at'];

    public function pumpable()
    {
        return $this->morphTo();
    }

    public function scopePumpDataCurrent($query, $parameterId){
        return $query->whereIn('parameter_id', $parameterId)->sortByDesc('recorded_date')->first();
    }

    public function scopeTest()
    {
        dump('I am test function');
    }
}
