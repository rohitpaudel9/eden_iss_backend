<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pump extends Model
{
    protected $guarded = ['id'];

    public function subsystem()
    {
        return $this->belongsTo(Pump::class);
    }

    public function data()
    {
        return $this->morphOne('App\pumpData', 'pumpable');
    }
}
