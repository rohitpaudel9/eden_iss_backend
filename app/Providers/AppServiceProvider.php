<?php

namespace App\Providers;

use App\Pump;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder; // Import Builder where defaultStringLength method is defined

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191); // Update defaultStringLength

        Validator::extend('poly_exists', function ($attribute, $value, $parameters, $validator) {
            if (!$objectType = Arr::get($validator->getData(), $parameters[0], false)) {
                return false;
            }
            if (!class_exists($objectType)) {
                return false;
            }
            return !empty(resolve($objectType)->find($value));
        });

        Relation::morphMap([
            Pump::class
        ]);
    }
}
