<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PumpResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
            'parameter_id' => (string)$this->parameter_id,
            'parameter_label' => (string)$this->parameter_label,
            'equipment_description' => (string)$this->equipment_description,
        ];
    }
}
