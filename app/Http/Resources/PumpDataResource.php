<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PumpDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'parameter_id' => (string)$this->parameter_id,
            'state' => (string)$this->state,
            'recorded_date' => (string)$this->recorded_date
        ];
    }
}
