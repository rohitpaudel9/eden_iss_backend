<?php

namespace App\Http\Controllers;

use App\Http\Resources\PumpDataResource;
use App\pumpData;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class PumpDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PumpDataResource::collection(pumpData::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataWithParameterId($parameterId)
    {
        //return PumpDataResource::collection(pumpData::PumpDataOfParameterId($parameter_id))->first();
        return pumpData::query()->whereIn('parameter_id', [$parameterId])->get();
    }

    public function currentData($parameterId)
    {

    }
}
