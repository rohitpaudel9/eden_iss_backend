<?php

namespace App\Service;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Validator;

class MorphObjectService
{
    public static function resolveObjectMorph($model, $id)
    {
        $model = Relation::getMorphedModel($model);

        Validator::make([
            'type' => $model,
            'id' => (int)$id,
        ], [
            'type' => 'required|string',
            'id' => 'required|integer|poly_exists:type'
        ])->validated();

        $object = resolve($model)->find(id);

        return $object;
    }
}
