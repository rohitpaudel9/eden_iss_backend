<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsystem extends Model
{
    protected $guarded = ['id'];

    public function pumps()
    {
        return $this->hasMany(Pump::class);
    }
}
