<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Solenoid;
use Faker\Generator as Faker;

$factory->define(Solenoid::class, function (Faker $faker) {
    return [
        'parameter_id' => $faker->randomNumber(5),
        'parameter_label' => $faker->sentence(4),
        'equipment_description' => $faker->sentence(6),
    ];
});
