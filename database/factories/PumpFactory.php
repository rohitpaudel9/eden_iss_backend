<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pump;
use App\Subsystem;
use Faker\Generator as Faker;

$factory->define(Pump::class, function (Faker $faker) {
    return [
        'parameter_id' => $faker->randomNumber(5),
        'subsystem_id' => Subsystem::all()->random(),
        'parameter_label' => $faker->sentence(3),
        'equipment_description' => $faker->sentence(5),
    ];
});
