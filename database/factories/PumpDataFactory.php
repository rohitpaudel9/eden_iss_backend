<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pump;
use App\pumpData;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Relations\Relation;

$factory->define(pumpData::class, function (Faker $faker) {
    $entity = $faker->randomElement([Pump::all()->random()]);
    $pump = Pump::all()->random();
    return [
        'state' => $faker->randomElement([1, 0]),
        'pumpable_id' => $entity->id,
        'parameter_id' => $pump->parameter_id,
        'recorded_time' => \Carbon\Carbon::now()->toDateString(),
        'pumpable_type' => collect(Relation::morphMap())->flip()[get_class($entity)],
    ];
});
