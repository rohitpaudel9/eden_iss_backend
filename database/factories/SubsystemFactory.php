<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subsystem;
use Faker\Generator as Faker;

$factory->define(Subsystem::class, function (Faker $faker) {
    return [
        'zone_prefix' => $faker->word,
        'name' => $faker->word,
    ];
});
