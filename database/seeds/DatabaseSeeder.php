<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(SubsystemSeeder::class);
        $this->call(PumpSeeder::class);
        $this->call(SolenoidSeeder::class);
        $this->call(PumpDataSeeder::class);
    }
}
