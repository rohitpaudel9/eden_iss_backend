<?php

use App\Subsystem;
use Illuminate\Database\Seeder;

class SubsystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Subsystem::class, 8)->create();
    }
}
