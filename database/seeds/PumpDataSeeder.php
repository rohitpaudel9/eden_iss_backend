<?php

use App\pumpData;
use Illuminate\Database\Seeder;

class PumpDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(pumpData::class, 20)->create();
    }
}
